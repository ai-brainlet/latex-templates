# Latex Templates 

## Latex Workflow

If you've never heard of it, Latex is markup for making pdf documents

- [ Learn more about Latex ](
https://www.latex-project.org/
)
- [ These templates are from this website ](
https://www.latextemplates.com/
)

My workflow consists of copying and editing template folders.
From there, it's just a matter of tweaking the template to taste,
editing the content, and then compiling the document.  





